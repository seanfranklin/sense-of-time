'use strict';

const gulp = require('gulp'),
    concat = require('gulp-concat'),
   postcss = require('gulp-postcss'),
      sass = require('gulp-sass'),
sourcemaps = require('gulp-sourcemaps'),
    terser = require('gulp-terser'),
  csswring = require('csswring');

gulp.task('styles', () => {
    return gulp.src(['src/styles/*.scss'])
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss([csswring({ removeAllComments: true })]))
        .pipe(concat('style.min.css'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('www/'));
});

gulp.task('scripts', () => {
    const srcFiles = [
        'src/scripts/mar.js',
        'src/scripts/olap.js',
        'src/scripts/elem.js',
        'src/scripts/opt.js',
        'src/scripts/sot.js',
        'src/scripts/init.js'
    ];
    return gulp.src(srcFiles)
        .pipe(sourcemaps.init())
        .pipe(terser())
        .pipe(concat('script.min.js'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('www/'));
});
