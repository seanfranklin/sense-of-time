'use strict';

const cacheName = 'sot_cache_v1',

    assets = [
        '/',
        'index.html',
        'style.min.css',
        'script.min.js',
        'images/arrow.svg',
        'images/mask.svg',
        'images/menu.svg',
        'images/concrete.png',
        'images/twinkle.png'
    ];

self.addEventListener('install', event => {
    console.info('Event: install service worker');
    event.waitUntil(caches.open(cacheName).then(cache => cache.addAll(assets)));
});

self.addEventListener('activate', event => {
    console.info('Event: activate service worker');
    event.waitUntil(caches.keys().then(keys => {
        return Promise.all(keys.map(key => {
            if (key !== cacheName) {
                console.info('Deleting old cache: ' + key);
                return caches.delete(key);
            }
        }));
    }).then(self.clients.claim()));
});

self.addEventListener('fetch', event => {
    console.info('Event: fetch ' + event.request.url);
    event.waitUntil(event.respondWith(caches.open(cacheName).then(cache => {
        return cache.match(event.request).then(cachedResponse => {
            cachedResponse && console.info('Asset found in cache');
            return cachedResponse || fetch(event.request).then(serverResponse => {
                console.info('Asset fetched from server');
                return cache.put(event.request, serverResponse.clone()).then(() => {
                    console.info('Asset added to cache');
                    return serverResponse;
                });
            });
        });
    })));
});
